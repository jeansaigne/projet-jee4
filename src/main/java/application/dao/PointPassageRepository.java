package application.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import application.models.PointPassage;


public interface PointPassageRepository extends CrudRepository<PointPassage, Long> {

    List<PointPassage> findByNom(String nom);
}