package application.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import application.dao.CustomerRepository;
import application.models.Customer;

@Controller
@RequestMapping("api/customers")
public class CustomerController {
	
	private static final Logger log = LoggerFactory.getLogger(CustomerController.class);
	
	@Autowired
	CustomerRepository customerRepository;
	
	@RequestMapping(path="", method=RequestMethod.GET)
    public String read( Model model) 
    {
		List<Customer> customers = (List<Customer>) customerRepository.findAll();
		model.addAttribute("customers", customers);
        return "customers";
    }
	
	@RequestMapping(path="save/{name}/{last}", method=RequestMethod.GET)
    public String read(@PathVariable(value="name") String name, @PathVariable(value="last") String last, Model model) 
    {
		Customer customer = new Customer();
		customer.setFirstName(name);
		customer.setLastName(last);
		customerRepository.save(customer);
		List<Customer> customers = (List<Customer>) customerRepository.findAll();
		
		model.addAttribute("customers", customers);
        return "customers";
    }
	
	@RequestMapping(path="delete/{name}", method=RequestMethod.GET)
    public String read(@PathVariable(value="name") String name, Model model) 
    {
		Customer customer = customerRepository.findByFirstName(name);
		customerRepository.delete(customer);
		List<Customer> customers = (List<Customer>) customerRepository.findAll();
		
		model.addAttribute("customers", customers);
        return "customers";
    }
	
	@RequestMapping(path="update/{name}/{newName}/{newLastName}", method=RequestMethod.GET)
    public String read(@PathVariable(value="name") String name, @PathVariable(value="newName") String newName, @PathVariable(value="newLastName") String newLastName, Model model) 
    {
		log.info("Rest request to update customer {}", name);
		Customer customer = null;
		try{
			customer = customerRepository.findByFirstName(name);
		}catch(Exception e){
			log.error("{}", e.fillInStackTrace());
			return "500";
		}
		if(customer == null){
			log.warn("Not Found");
			return "404";
		}
		customer.setFirstName(newName);
		customer.setLastName(newLastName);
		customerRepository.save(customer);
		List<Customer> customers = (List<Customer>) customerRepository.findAll();
		
		model.addAttribute("customers", customers);
        return "customers";
    }

//    @RequestMapping(value="login", method=RequestMethod.GET)
//    public String create()
//    {
//        return "account/login";
//    }
//    
//    @RequestMapping(value="register", method=RequestMethod.GET)
//    public String update()
//    {
//        return "account/register";
//    }
//
//    @RequestMapping(value="login", method=RequestMethod.GET)
//    public String delate()
//    {
//        return "account/login";
//    }
	
}
