function initMap() {
	var mapDiv = document.getElementById('gmap');
  var map = new google.maps.Map(mapDiv, {
    center: {lat: -34.397, lng: 150.644},
    zoom: 6
  });
  map.addListener('click', function(e) {
	  console.log(e,map);
	    placeMarkerAndPanTo(e.latLng, map);
	  });
  var infoWindow = new google.maps.InfoWindow({map: map});

  // Try HTML5 geolocation.
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      infoWindow.setPosition(pos);
      infoWindow.setContent('Location found.');
      map.setCenter(pos);
    }, function() {
      handleLocationError(true, infoWindow, map.getCenter());
    });
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, infoWindow, map.getCenter());
  }
  
}

function placeMarkerAndPanTo(latLng, map) {
	document.getElementById("latitude").value = latLng.lat().toFixed(5);
	document.getElementById("longitude").value = latLng.lng().toFixed(5);
	  var marker = new google.maps.Marker({
	    position: latLng,
	    map: map
	  });
	  map.panTo(latLng);
	}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
}