package application.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import application.models.Customer;


public interface CustomerRepository extends CrudRepository<Customer, Long> {

    List<Customer> findByLastName(String lastName);
    
    Customer findByFirstName(String firstName);
}