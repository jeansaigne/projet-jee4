#Projet JEE

##Mise en place pour eclipse

  - Importer le projet dans eclipse
  - Dans le dossier du projet:
  
		mvn dependency:resolve
		mvn eclipse:eclipse
  		
##Lancer l'application

		mvn spring-boot:run
		
Configurer eclipse pour run, clique droit sur le projet, "Run as" > "Maven build...", puis dans l'input "goals", saisir la commande "spring-boot:run"

##Créer et lancer un executable jar standalone

		mvn package
		jarva -jar package.jar
