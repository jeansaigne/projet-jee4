package application.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import application.dao.PointPassageRepository;
import application.models.PointPassage;

@Controller
@RequestMapping("pointpassages")
public class PointPassageController {

	@Autowired
	PointPassageRepository pointPassageRepository;
	
	@RequestMapping(path="", method=RequestMethod.GET)
    public String list(Model model) 
    {
		List<PointPassage> pointPassages = (List<PointPassage>) pointPassageRepository.findAll();
		model.addAttribute("pointPassages", pointPassages);
        return "pointpassages/list";
    }
	
	@RequestMapping(path="", method=RequestMethod.POST)
    public String create(
    		@RequestParam(value="nom") String nom,
    		@RequestParam(value="latitude") double latitude,
    		@RequestParam(value="longitude") double longitude,
    		@RequestParam(value="description") String description,
    		Model model) 
    {
		List<PointPassage> pointPassages = (List<PointPassage>) pointPassageRepository.findAll();
		model.addAttribute("pointPassages", pointPassages);
        return "pointpassages/list";
    }
	
	@RequestMapping(path="add", method=RequestMethod.GET)
    public String add(Model model) 
    {
		List<PointPassage> pointPassages = (List<PointPassage>) pointPassageRepository.findAll();
		model.addAttribute("pointPassages", pointPassages);
        return "pointpassages/add";
    }
}
